package com.example.student.databasesourceit;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface PeopleDao {

    @Insert
    void insertAll(People... people);

    @Delete
    void delete(People person);

    @Query("SELECT * FROM people")
    List<People> getAllPeople();

    @Query("SELECT * FROM people WHERE age > :age")
    List<People> getAdultPeople(int age);

    @Query("DELETE FROM people")
    void deleteAll();

}
