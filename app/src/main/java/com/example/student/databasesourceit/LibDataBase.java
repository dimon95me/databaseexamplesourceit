package com.example.student.databasesourceit;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {People.class}, version = 1)
public abstract class LibDataBase extends RoomDatabase {
    public abstract PeopleDao getPeopleDao();
}
