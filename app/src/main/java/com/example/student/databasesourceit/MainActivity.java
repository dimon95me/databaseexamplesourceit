package com.example.student.databasesourceit;

import android.arch.persistence.room.Room;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.List;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "my_tag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final LibDataBase db = Room.databaseBuilder(getApplicationContext(), LibDataBase.class, "people-database").build();

        Observable.just(db)
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .map(new Func1<LibDataBase, List<People>>() {
                    @Override
                    public List<People> call(LibDataBase libDataBase) {
                        db.getPeopleDao().deleteAll();
                        for (int i = 0; i < 100; i++) {
                            int k = i;
                            db.getPeopleDao().insertAll(new People(i, "Human " + i, k % 20, "City " + i));
                        }

                        List<People> peopleList = db.getPeopleDao().getAdultPeople(18);

                        return peopleList;
                    }
                })
                .subscribe(new Action1<List<People>>() {
                    @Override
                    public void call(List<People> people) {
                        List<People> peopleList = db.getPeopleDao().getAdultPeople(18);
                        for (People people1 : people) {
                            Log.d(TAG, people1.toString());
                        }
                    }
                });
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                db.getPeopleDao().deleteAll();
//                for (int i = 0; i < 100; i++) {
//                    int k = i;
//                    db.getPeopleDao().insertAll(new People(i,"Human "+i, k%20, "City "+i));
//                }
//
//                List<People> peopleList = db.getPeopleDao().getAdultPeople(18);
//                for (People people : peopleList) {
//                    Log.d(TAG, people.toString());
//                }
//            }
//        }).start();
    }
}
